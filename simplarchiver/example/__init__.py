from .sleep import SleepFeeder, SleepDownloader
from .random import RandomFeeder
from .just import JustDownloader